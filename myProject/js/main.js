document.getElementById("hello_text").textContent = "はじめてのJavaScript";

var count = 0;

var cells;

// ブロックのパターン
var blocks = {
    i: {
      class: "i",
      pattern: [
        [1, 1, 1, 1]
      ]
    },
    o: {
      class: "o",
      pattern: [
        [1, 1], 
        [1, 1]
      ]
    },
    t: {
      class: "t",
      pattern: [
        [0, 1, 0], 
        [1, 1, 1]
      ]
    },
    s: {
      class: "s",
      pattern: [
        [0, 1, 1], 
        [1, 1, 0]
      ]
    },
    z: {
      class: "z",
      pattern: [
        [1, 1, 0], 
        [0, 1, 1]
      ]
    },
    j: {
      class: "j",
      pattern: [
        [1, 0, 0], 
        [1, 1, 1]
      ]
    },
    l: {
      class: "l",
      pattern: [
        [0, 0, 1], 
        [1, 1, 1]
      ]
    }
  };

loadTable();

setInterval(function() {
  count++;
  document.getElementById("hello_text").textContent = "はじめてのJavaScript(" + count + ")";
  
  if(hasFallingBlock()){
    fallBlocks();
  }else{
      deleteRow();

      // ブロックが積み上がり切っていないかのチェック
        for (var row = 0; row < 2; row++) {
            for (var col = 0; col < 10; col++) {
                if (cells[row][col].className !== "") {
                    alert("game over");
                }
            }
        }

      generateBlock();
  }
},1000);

function loadTable(){
  var td_array = document.getElementsByTagName("td");
  cells = [];
  var index = 0;
  for (var row = 0; row < 20; row++) {
    cells[row] = [];
    for (var col = 0; col < 10; col++) {
      cells[row][col] = td_array[index];
      index++;
    }
  }

}

function fallBlocks(){
  // 1. 底についていないか？
  for (var col = 0; col < 10; col++) {
    if (cells[19][col].blockNum === fallingBlockNum) {
      isFalling = false;
      return; // 一番下の行にブロックがいるので落とさない
    }
  }
  // 2. 1マス下に別のブロックがないか？
  for (var row = 18; row >= 0; row--) {
    for (var col = 0; col < 10; col++) {
      if (cells[row][col].blockNum === fallingBlockNum) {
        if (cells[row + 1][col].className !== "" && cells[row + 1][col].blockNum !== fallingBlockNum){
          isFalling = false;
          return; // 一つ下のマスにブロックがいるので落とさない
        }
      }
    }
  }
  /*
  // 一番下の行のクラスを空にする
  for (var i = 0; i < 10; i++) {
    if(cells[19][i].className != ""){
        isFalling = false;
        return;
    }
  }
  */
  // 下から二番目の行から繰り返しクラスを下げていく
  for (var row = 18; row >= 0; row--) {
    for (var col = 0; col < 10; col++) {
      if (cells[row][col].blockNum === fallingBlockNum) {
        cells[row + 1][col].className = cells[row][col].className;
        cells[row + 1][col].blockNum = cells[row][col].blockNum;
        cells[row][col].className = "";
        cells[row][col].blockNum = null;
      }
    }
  }
}

var isFalling = false;
function hasFallingBlock() {
    // 落下中のブロックがあるか確認する
    return isFalling;
  }

function deleteRow() {
    // そろっている行を消す
  }
  
  var fallingBlockNum = 0;
  function generateBlock() {
    // ランダムにブロックを生成する
    // 1. ブロックパターンからランダムに一つパターンを選ぶ
    var keys = Object.keys(blocks);
    var nextBlockKey = keys[Math.floor(Math.random() * keys.length)];
    var nextBlock = blocks[nextBlockKey];
    var nextFallingBlockNum = fallingBlockNum + 1;
    // 2. 選んだパターンをもとにブロックを配置する
    var pattern = nextBlock.pattern;
    for (var row = 0; row < pattern.length; row++) {
        for (var col = 0; col < pattern[row].length; col++) {
            if (pattern[row][col]) {
                cells[row][col + 3].className = nextBlock.class;
                cells[row][col + 3].blockNum = nextFallingBlockNum;
            }
        }
    }
    // 3. 落下中のブロックがあるとする
    isFalling = true;
    fallingBlockNum = nextFallingBlockNum;
  }
  
  function moveRight() {

    //右側が壁、または他のブロックではないか?
    for (var row = 0; row < 20; row++) {
        for (var col = 0; col < 10; col++) {
            if (cells[row][col].blockNum === fallingBlockNum) {
                if(col == 9){
                    return;
                }
                else if(cells[row][col + 1].className !== "" && cells[row][col + 1].blockNum !== fallingBlockNum){
                    return;
                }
            }
        }
    }

    // ブロックを右に移動させる
    for (var row = 0; row < 20; row++) {
      for (var col = 9; col >= 0; col--) {
        if (cells[row][col].blockNum === fallingBlockNum) {
          cells[row][col + 1].className = cells[row][col].className;
          cells[row][col + 1].blockNum = cells[row][col].blockNum;
          cells[row][col].className = "";
          cells[row][col].blockNum = null;
        }
      }
    }
  }
  
  function moveLeft() {

    //左側が壁、または他のブロックではないか?
    for (var row = 0; row < 20; row++) {
        for (var col = 0; col < 10; col++) {
            if (cells[row][col].blockNum === fallingBlockNum) {
                if(col == 0){
                    return;
                }
                else if(cells[row][col - 1].className !== "" && cells[row][col - 1].blockNum !== fallingBlockNum){
                    return;
                }
            }
        }
    }

    // ブロックを左に移動させる
    for (var row = 0; row < 20; row++) {
      for (var col = 0; col < 10; col++) {
        if (cells[row][col].blockNum === fallingBlockNum) {
          cells[row][col - 1].className = cells[row][col].className;
          cells[row][col - 1].blockNum = cells[row][col].blockNum;
          cells[row][col].className = "";
          cells[row][col].blockNum = null;
        }
      }
    }
  }

  function deleteRow() { //思ったように落ちない
    // そろっている行を消す
    for (var row = 19; row >= 0; row--) {
      var canDelete = true;
      for (var col = 0; col < 10; col++) {
        if (cells[row][col].className === "") {
          canDelete = false;
        }
      }
      if (canDelete) {
        // 1行消す
        for (var col = 0; col < 10; col++) {
          cells[row][col].className = "";
        }
        // 上の行のブロックをすべて1マス落とす
        for (var downRow = row - 1; row >= 0; row--) {
          for (var col = 0; col < 10; col++) {
            cells[downRow + 1][col].className = cells[downRow][col].className;
            cells[downRow + 1][col].blockNum = cells[downRow][col].blockNum;
            cells[downRow][col].className = "";
            cells[downRow][col].blockNum = null;
          }
        }
      }
    }
  }

  // キーボードイベントを監視する
document.addEventListener("keydown", onKeyDown);

// キー入力によってそれぞれの関数を呼び出す
function onKeyDown(event) {
  if (event.keyCode === 37) {
    moveLeft();
  } else if (event.keyCode === 39) {
    moveRight();
  } else if (event.keyCode === 40) {
        count++;
        document.getElementById("hello_text").textContent = "はじめてのJavaScript(" + count + ")";
        
        if(hasFallingBlock()){
            fallBlocks();
        }else{
            deleteRow();
      
            // ブロックが積み上がり切っていないかのチェック
              for (var row = 0; row < 2; row++) {
                  for (var col = 0; col < 10; col++) {
                      if (cells[row][col].className !== "") {
                          alert("game over");
                      }
                  }
              }
      
            generateBlock();
        }
  }
}